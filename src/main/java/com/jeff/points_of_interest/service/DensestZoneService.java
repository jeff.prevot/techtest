package com.jeff.points_of_interest.service;

import com.jeff.points_of_interest.domain.model.PointOfInterest;
import com.jeff.points_of_interest.domain.model.Zone;
import com.jeff.points_of_interest.dto.response.ZoneDTO;
import com.jeff.points_of_interest.infrastructure.PoiDAO;
import com.jeff.points_of_interest.infrastructure.service.MapPoiInZone;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class DensestZoneService {

  private PoiDAO poiDAO;
  private MapPoiInZone mapPoiInZone;

  public DensestZoneService(final PoiDAO poiDAO, final MapPoiInZone mapPoiInZone) {
    this.poiDAO = poiDAO;
    this.mapPoiInZone = mapPoiInZone;
  }

  public List<ZoneDTO> execute(final Integer maxResult) throws IOException {
    final Map<Zone, Integer> nbOfPoiByZone = getNbOfPoiByZone();
    final List<Zone> sortedZonesByNbOfPoi = nbOfPoiByZone.entrySet()
                                                         .stream()
                                                         .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                                                         .map(Entry::getKey).collect(Collectors.toList());
    final List<Zone> zones = sortedZonesByNbOfPoi.stream()
                                                 .limit(maxResult)
                                                 .collect(Collectors.toList());
    return zones.stream()
                .map(zone -> new ZoneDTO(zone.getMin_lat(), zone.getMax_lat(), zone.getMin_lon(), zone.getMax_lon()))
                .collect(Collectors.toList());
  }

  private Map<Zone, Integer> getNbOfPoiByZone() throws IOException {
    final List<PointOfInterest> pointOfInterests = poiDAO.getAll();
    final Map<Zone, Integer> nbOfPoiByZone = new HashMap<>();
    for (PointOfInterest pointOfInterest : pointOfInterests) {
      final Zone zone = mapPoiInZone.execute(pointOfInterest);
      nbOfPoiByZone.putIfAbsent(zone, 0);
      nbOfPoiByZone.put(zone, nbOfPoiByZone.get(zone) + 1);
    }
    return nbOfPoiByZone;
  }
}
