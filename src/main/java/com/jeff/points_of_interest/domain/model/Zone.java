package com.jeff.points_of_interest.domain.model;

import java.util.Objects;

public class Zone {

  private Double min_lat;
  private Double max_lat;
  private Double min_lon;
  private Double max_lon;

  public Zone(final Double min_lat, final Double max_lat, final Double min_lon, final Double max_lon) {
    this.min_lat = min_lat;
    this.max_lat = max_lat;
    this.min_lon = min_lon;
    this.max_lon = max_lon;
  }

  public Double getMin_lat() {
    return min_lat;
  }

  public Double getMax_lat() {
    return max_lat;
  }

  public Double getMin_lon() {
    return min_lon;
  }

  public Double getMax_lon() {
    return max_lon;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Zone)) {
      return false;
    }
    final Zone zone = (Zone) o;
    return Objects.equals(min_lat, zone.min_lat) &&
           Objects.equals(max_lat, zone.max_lat) &&
           Objects.equals(min_lon, zone.min_lon) &&
           Objects.equals(max_lon, zone.max_lon);
  }

  @Override
  public int hashCode() {
    return Objects.hash(min_lat, max_lat, min_lon, max_lon);
  }

  @Override
  public String toString() {
    return "Zone{" +
           "min_lat=" + min_lat +
           ", max_lat=" + max_lat +
           ", min_lon=" + min_lon +
           ", max_lon=" + max_lon +
           '}';
  }
}
