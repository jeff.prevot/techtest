package com.jeff.points_of_interest.controller;

import com.jeff.points_of_interest.dto.response.NbPoiDTO;
import com.jeff.points_of_interest.dto.response.ZoneDTO;
import com.jeff.points_of_interest.infrastructure.model.PointOfInterestSearchCriteria;
import com.jeff.points_of_interest.infrastructure.service.CommandLineParser;
import com.jeff.points_of_interest.service.DensestZoneService;
import com.jeff.points_of_interest.service.NbPoiService;

import java.io.IOException;
import java.util.List;

public class PointOfInterestController {

  private CommandLineParser commandLineParser;
  private NbPoiService nbPoiService;
  private DensestZoneService densestZoneService;

  public PointOfInterestController(final CommandLineParser commandLineParser, final NbPoiService nbPoiService, final DensestZoneService densestZoneService) {
    this.commandLineParser = commandLineParser;
    this.nbPoiService = nbPoiService;
    this.densestZoneService = densestZoneService;
  }

  public Object executeCommandLine(final String[] args) throws IOException {
    if ("--nbpoi".equals(args[0])) {
      return executeNbPoi(args);
    } else if ("--densest".equals(args[0])) {
      return executeDensest(args);
    } else {
      throw new UnsupportedOperationException();
    }
  }

  private NbPoiDTO executeNbPoi(final String[] args) throws IOException {
    final PointOfInterestSearchCriteria searchCriteria = commandLineParser.parseZoneParameter(args);
    return nbPoiService.execute(searchCriteria);
  }

  private List<ZoneDTO> executeDensest(final String[] args) throws IOException {
    final Integer maxResult = commandLineParser.parseMaxResult(args);
    return densestZoneService.execute(maxResult);
  }
}
