package com.jeff.points_of_interest.dto.response;

public class NbPoiDTO {

  private Integer value;

  public NbPoiDTO(final Integer value) {
    this.value = value;
  }
}
