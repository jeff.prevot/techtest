package com.jeff.points_of_interest.infrastructure.service;

import com.jeff.points_of_interest.domain.model.PointOfInterest;
import com.jeff.points_of_interest.domain.model.Zone;

public class MapPoiInZone {

  private RoundHalfDownService roundHalfDownService;

  public MapPoiInZone(final RoundHalfDownService roundHalfDownService) {
    this.roundHalfDownService = roundHalfDownService;
  }

  public Zone execute(final PointOfInterest pointOfInterest) {
    final Double lat = roundHalfDownService.execute(pointOfInterest.getLat());
    final Double lon = roundHalfDownService.execute(pointOfInterest.getLon());
    return new Zone(lat, lat + 0.5, lon, lon + 0.5);
  }
}
