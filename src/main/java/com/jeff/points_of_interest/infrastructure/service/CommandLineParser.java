package com.jeff.points_of_interest.infrastructure.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jeff.points_of_interest.infrastructure.model.PointOfInterestSearchCriteria;

import java.util.Arrays;

public class CommandLineParser {

  private Gson gson;

  public CommandLineParser(final Gson gson) {
    this.gson = gson;
  }

  public Integer parseMaxResult(final String[] args) {
    final JsonObject jsonObject = convertInJson(args);
    return jsonObject.get("n").getAsInt();
  }

  public PointOfInterestSearchCriteria parseZoneParameter(final String[] args) {
    final JsonObject jsonObject = convertInJson(args);
    final Double min_lat = jsonObject.get("min_lat").getAsDouble();
    final Double min_lon = jsonObject.get("min_lon").getAsDouble();
    return new PointOfInterestSearchCriteria(min_lat, min_lon);
  }

  private JsonObject convertInJson(final String[] args) {
    final String fullParameters = String.join("", Arrays.copyOfRange(args, 1, args.length));
    final String parameter = fullParameters.substring(1, fullParameters.length() - 1);
    return gson.fromJson(parameter, JsonObject.class);
  }
}