package com.jeff.points_of_interest.infrastructure;

import com.jeff.points_of_interest.domain.model.PointOfInterest;
import com.jeff.points_of_interest.infrastructure.model.PointOfInterestSearchCriteria;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PoiDAO {

  private static final String POINT_OF_INTERESTS_FILE_PATH = "PointOfInterests.tsv";

  public List<PointOfInterest> getAll() throws IOException {
    return findBySearchCriteria(new PointOfInterestSearchCriteria(null, null));
  }

  public List<PointOfInterest> findBySearchCriteria(final PointOfInterestSearchCriteria searchCriteria) throws IOException {
    try (BufferedReader br = new BufferedReader(new FileReader(POINT_OF_INTERESTS_FILE_PATH))) {
      return br.lines()
               .skip(1)
               .map(line -> this.mapToItem(line, searchCriteria))
               .filter(Objects::nonNull)
               .collect(Collectors.toList());
    }
  }

  private PointOfInterest mapToItem(final String line, final PointOfInterestSearchCriteria searchCriteria) {
    final String[] columns = line.split("\t");
    final String id = columns[0];
    final Double lat = Double.parseDouble(columns[1]);
    final Double lon = Double.parseDouble(columns[2]);
    if (inIncludedInExpectedZone(lat, lon, searchCriteria)) {
      return new PointOfInterest(id, lat, lon);
    }
    return null;
  }

  private boolean inIncludedInExpectedZone(final Double lat, final Double lon, final PointOfInterestSearchCriteria searchCriteria) {
    final Double searchCriteriaMinLat = searchCriteria.getMinLat();
    final Double searchCriteriaMinLon = searchCriteria.getMinLon();
    return (searchCriteriaMinLat == null || (lat >= searchCriteriaMinLat && lat <= (searchCriteriaMinLat + 0.5)))
           &&
           (searchCriteriaMinLon == null || (lon >= searchCriteriaMinLon && lon <= (searchCriteriaMinLon + 0.5)));
  }
}
