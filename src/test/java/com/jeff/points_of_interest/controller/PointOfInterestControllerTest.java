package com.jeff.points_of_interest.controller;

import com.jeff.points_of_interest.dto.response.NbPoiDTO;
import com.jeff.points_of_interest.dto.response.ZoneDTO;
import com.jeff.points_of_interest.infrastructure.service.CommandLineParser;
import com.jeff.points_of_interest.infrastructure.model.PointOfInterestSearchCriteria;
import com.jeff.points_of_interest.service.DensestZoneService;
import com.jeff.points_of_interest.service.NbPoiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PointOfInterestControllerTest {

  @Mock
  private CommandLineParser commandLineParser;
  @Mock
  private NbPoiService nbPoiService;
  @Mock
  private DensestZoneService densestZoneService;
  @Mock
  private PointOfInterestSearchCriteria pointOfInterestSearchCriteria;
  @Mock
  private NbPoiDTO nbPoiDTO;
  @InjectMocks
  private PointOfInterestController pointOfInterestController;

  @Test
  void execute_nbpoi_command_line() throws IOException {
    final String[] args = {"--nbpoi", "'{\"min_lat\": 6.5,\"min_lon\": -7}'\""};
    when(commandLineParser.parseZoneParameter(args)).thenReturn(pointOfInterestSearchCriteria);
    when(nbPoiService.execute(pointOfInterestSearchCriteria)).thenReturn(nbPoiDTO);

    Object dto = pointOfInterestController.executeCommandLine(args);

    verify(commandLineParser).parseZoneParameter(args);
    verify(commandLineParser, never()).parseMaxResult(any());
    verify(nbPoiService).execute(pointOfInterestSearchCriteria);
    verify(densestZoneService, never()).execute(any());
    assertThat(dto).isEqualTo(nbPoiDTO);
  }

  @Test
  void execute_denset_command_line() throws IOException {
    final Integer maxResult = 3;
    final String[] args = {"--densest", "'{\"n\": 2}'"};
    final List<ZoneDTO> expectedZones = new ArrayList<>();
    when(commandLineParser.parseMaxResult(args)).thenReturn(maxResult);
    when(densestZoneService.execute(maxResult)).thenReturn(expectedZones);

    Object dto = pointOfInterestController.executeCommandLine(args);

    verify(commandLineParser).parseMaxResult(args);
    verify(commandLineParser, never()).parseZoneParameter(args);
    verify(densestZoneService).execute(maxResult);
    verify(nbPoiService, never()).execute(pointOfInterestSearchCriteria);
    assertThat(dto).isEqualTo(expectedZones);
  }
}