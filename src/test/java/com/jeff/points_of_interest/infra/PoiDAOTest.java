package com.jeff.points_of_interest.infra;

import com.jeff.points_of_interest.domain.model.PointOfInterest;
import com.jeff.points_of_interest.infrastructure.model.PointOfInterestSearchCriteria;
import com.jeff.points_of_interest.infrastructure.PoiDAO;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class PoiDAOTest {

  private final static PointOfInterest POI1 = new PointOfInterest("id1", -48.6, -37.7);
  private final static PointOfInterest POI2 = new PointOfInterest("id2", -27.1, 8.4);
  private final static PointOfInterest POI3 = new PointOfInterest("id3", 6.6, -6.9);
  private final static PointOfInterest POI4 = new PointOfInterest("id4", -2.3, 38.3);
  private final static PointOfInterest POI5 = new PointOfInterest("id5", 6.8, -6.9);
  private final static PointOfInterest POI6 = new PointOfInterest("id6", -2.5, 38.3);
  private final static PointOfInterest POI7 = new PointOfInterest("id7", 0.1, -0.1);
  private final static PointOfInterest POI8 = new PointOfInterest("id8", -2.1, 38.1);
  private final static List<PointOfInterest> allPOIs = Arrays.asList(POI1, POI2, POI3, POI4, POI5, POI6, POI7, POI8);
  private PoiDAO poiDAO = new PoiDAO();

  @Test
  void return_id3_and_id5() throws IOException {
    final List<PointOfInterest> pointOfInterests = poiDAO.findBySearchCriteria(new PointOfInterestSearchCriteria(6.5, -7.0));

    assertThat(pointOfInterests).containsExactly(POI3, POI5);
  }

  @Test
  void return_id4_and_id6_and_id8() throws IOException {
    final List<PointOfInterest> pointOfInterests = poiDAO.findBySearchCriteria(new PointOfInterestSearchCriteria(-2.5, 38.0));

    assertThat(pointOfInterests).containsExactly(POI4, POI6, POI8);
  }

  @Test
  void return_all_POIs() throws IOException {
    final List<PointOfInterest> pointOfInterests = poiDAO.getAll();

    assertThat(pointOfInterests).containsExactlyElementsOf(allPOIs);
  }
}