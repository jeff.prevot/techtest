# POINTS_OF_INTEREST
This file doesn't have a traditional purpose as I will strive to justify my choices 

## Description
The purpose of this app is to calculate the population density from a provided TSV file.<br>
This file, named _PointOfInterests.tsv_, is located at the root of this project.

## General Technical Documentation
I made my best to implement the DDD pattern. So the application is split as following:
* As an entry-point, the _Main_ class.<br>
It only instanciates the services later used to satisfy the specified need, then hand over a controller.
* The _controller_ package contains only a controller (sic) used as orchestrator, according to the provided command line.<br>
>We notice this controller return a Java _Object_, that the _Main_ class will convert in JSON format. This would be avoid in a RESTfull app, as the return format would be configured with an _ObjectMapper_, for example.
* Then two different services are responsible of the business itself. I would call them _business services_<br>
When a first one returns the number of Point Of Interest included in a provided zone, the second calculates the densest zones.
* These services calls a DAO class, that is the link with the datat storage.<br>
If we would change the data storage, only this DAO would have to be modified. We sure are able to imagine this app running side of a NoSQL server, or even different kind of storage in the same time. This DAO would handle these choices.
* Next to this, some infrastructure services encapsulate logic serving the _business services_.
* Finally, when the _domain_ package includes the model objects, the _dtos_ are the types returned by the _business services_. This avoiding the expose the business to the outside of the app.

### Detailed Technical Documentation
Here I'll try to clarify some specific choices.<br><br>
We can see two different usages of the DAO.<br>
* The first _business service_, _DensestZoneService_, ask it **all** the stored entries in the TSV file. Then it compute this data looking for the densest zones. This cannot be possible to have this information without comparing all the Point Of Interest zone one by one.<br><br>
In our case, with very few entries, this choice is acceptable. I sure won't be if we have to manage some thousands, or even million, entries.<br>
In this case, we would partition the data request, and by paralleling the calculus for each partition, we would then compare each result. It would be a nice case for multi-threading calculus.
<br><br>
* The second *business service*, _NbPoiService_, ask to the DAO only the useful data.<br>
To be clear, it provide to the DAO the parameter the user give in the command line. These parameter are encapsulated in a _SearchCriteria_ object, that can then be used by the DAO to filter data it have to return.<br><br>
Typically, if we would use a relational db, these searchCriteria would be used in a _WHERE_ clause.<br>
Here, with a file system storage, we filter the data at the moment of the model instanciation. Iterating through all the stored entries, we instanciate a Point Of Interest only if the read entry correspond to the search criteria.<br>

## Miscellaneous
* If it matters, I used JetBrains IntelliJ IDEA
* This project took me about 15 hours, specifications and this README redaction included.
* As i'm not comfortable setting up a REST infrastructure from scratch, I did prefer to stay with the Command Line Interface. The result would have been much better as I'm used to code REST end-points, but it would take so mush time that it would not fit with the deadline of friday 25/10/19.<br>
However, I create two methods in the controller as to be ready to receive the annotations () to set it as the REST entry-points.<br>
The URLs would be like:
    * techtest/pointofinterests/denset?n=2
    * techtest/pointofinterests/nbpoi?min_lat=6.5&min_lon=-7